require('dotenv').config();
const TelegramBot = require('node-telegram-bot-api');
const ms = require('ms');

const token = process.env.TOKEN_TELEGRAM;
const bot = new TelegramBot(token, {polling: true});

// bot.on('new_chat_title', async (data) => console.log(data));

bot.on('message', async (data) => {

    const chat_id = data.chat.id;
    const text = data.text;
    const user_id = data.from.id;
    const message_id = data.message_id;


    // console.log(await bot.getChatAdministrators(chat_id))
    // console.log(text.toLowerCase().indexOf('farmfb.store'))
    // console.log(user_id)
    // console.log(text)

    const form = {
        until_date: Date.now() + ms('1m')
    };

    const triggers = [
        'farmfb.store',
        'farmfb',
        'привет',
        'пока',
        'лох'
    ];

    if (typeof (text) === "undefined") {
        return false;
    }
    for (let trigger of triggers) {
        if (text.toLowerCase().indexOf(trigger) != -1) {
            // bot.kickChatMember(chat_id, user_id, form)
            await bot.deleteMessage(chat_id, message_id);
        }
    }
})