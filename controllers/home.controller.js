class HomeController {
    index(req, res) {
        const triggers = [
            'farmfb.store',
            'farmfb',
            'привет',
            'пока',
            'лох',
        ];
        res.render('home', {trigger:triggers});
    }
}

module.exports = new HomeController()