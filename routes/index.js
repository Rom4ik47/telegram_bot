const express = require('express'),
    router = express.Router(),
    HomeController = require("../controllers/home.controller"),
    SaveWordController = require("../controllers/saveWord.controller");

router.route('/').get(HomeController.index)
router.route('/save_word').get(SaveWordController.index)
router.route('/save_word').post(SaveWordController.index2)


module.exports = router