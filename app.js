const express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    handlebars = require('express-handlebars'),
    routes = require('./routes/index');

app.use(bodyParser.urlencoded({extends: true}))
app.engine('handlebars', handlebars.engine({
    defaultLayout: 'main'
}))
app.set('view engine', 'handlebars')

app.use('/', routes)

// app.all('*', function(req, res){
//     res.status(404)
//     res.render('404')
// });

const server = app.listen(8081, function () {
    const host = server.address().address
    const port = server.address().port

    console.log(`Example app listening on port http://localhost:${port}`)
    console.log("Example app listening at http://%s:%s", host, port)
})


/**
 * import express from 'express';
 * import {engine} from 'express-handlebars';
 * import bodyParser from 'body-parser';
 *
 * const app = express();
 * // const port = 3000
 * app.use(bodyParser.urlencoded({extends: true}))
 * app.engine('handlebars', engine({
 *     defaultLayout: 'main'
 * }));
 * app.set('view engine', 'handlebars');
 *
 * app.get('/', (req, res) => {
 *     res.render('home');
 * });
 *
 * app.get('/save_word', function (req, res) {
 *     let response = {
 *         search_word: req.query.search_word
 *     }
 *     res.end(JSON.stringify(response));
 * })
 *
 * app.use((err, req, res, next) => {
 *     console.error(err.message)
 *     res.status(500)
 *     res.render('500')
 * })
 *
 * app.use((err, req, res, next) => {
 *     res.status(404)
 *     res.render('404')
 * })
 */

/**
 * import dotenv from 'dotenv';
 * dotenv.config();
 */

/**
 * import mysql from 'mysql';
 * Коннекты с mysql
 * const connection = mysql.createConnection({
 *     host: process.env.DB_CONNECTION,
 *     user: process.env.DB_USERNAME,
 *     database: process.env.DB_DATABASE,
 *     password: process.env.DB_PASSWORD
 * });
 * connection.connect(function (err) {
 *     if (err) {
 *         console.log(err)
 *     } else {
 *         console.log("Подключение к серверу MySQL успешно установлено");
 *     }
 * });
 * const user = "Tom";
 * // const sql = "INSERT INTO partners(name) VALUES(?)";
 * const sql = "SELECT * FROM partners";
 *
 * connection.query(sql, user, function (err, results) {
 *     if (err) console.log(err);
 *     console.log(results);
 * });
 * connection.end();
 */